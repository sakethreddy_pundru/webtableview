//
//  ViewController.m
//  TableViewWeb
//
//  Created by admin on 21/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
#import "URLViewController.h"





@interface ViewController ()

@end


@implementation ViewController{
    
    NSString *siteUrl;
    NSArray *webSitesNames;
    NSArray *webSitesURL;
    

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    webSitesURL = [[NSArray alloc]initWithObjects:@"https://accounts.google.com/",@"https://in.yahoo.com/?p=us",@"https://www.facebook.com/",@"https://www.youtube.com/",@"https://www.whatsapp.com/",@"https://twitter.com/?lang=en",@"https://bitbucket.org/",nil];
    
    webSitesNames = [[NSArray alloc]initWithObjects:@"Gmail",@"Yahoo",@"Facebook",@"Youtube",@"Whatsapp",@"Twitter",@"Bitbucket", nil];

    
    
    UITableView *tView;
    tView = [[UITableView alloc]init];
    tView.frame = self.view.frame;
    
    tView.delegate = self;
    tView.dataSource = self;
    
    [self.view addSubview:tView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [webSitesNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (nil == cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.text=[webSitesNames objectAtIndex:indexPath.row];
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    siteUrl=[webSitesURL objectAtIndex:indexPath.row];
    
    URLViewController *url = [[URLViewController alloc]init];
    url.siteUrl = siteUrl;
    [self.navigationController pushViewController:url animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
