//
//  URLViewController.m
//  TableViewWeb
//
//  Created by admin on 21/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "URLViewController.h"

@interface URLViewController ()

@end

@implementation URLViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    NSURL *url = [NSURL URLWithString:self.siteUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    UIWebView *web = [[UIWebView alloc]init];
    web.frame = self.view.frame;
    [web loadRequest:urlRequest];
    [self.view addSubview:web];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
