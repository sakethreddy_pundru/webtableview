//
//  URLViewController.h
//  TableViewWeb
//
//  Created by admin on 21/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URLViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic, retain) NSString *siteUrl;

@end
